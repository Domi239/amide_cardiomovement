Please make sure MatLab R2020a or higher is installed on your device.
For running the software you need following toolboxes installed:
1) Computer Vision Toolbox
2) Image Processing Toolbox
3) Curve Fitting Toolbox

To download toolboxes please navigate to the "APPS" tab and click "Get More Apps" to open the Add-On Explorer.